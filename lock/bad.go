package lock

import "sync"

type Demo2 struct {
	mu  sync.Mutex // 共享资源的锁
	res int        // 共享资源
}

func (d *Demo2) Read1() int {
	d.mu.Lock()
	res := d.res
	d.mu.Unlock()
	return res
}

func (d *Demo2) Read2() int {
	d.mu.Lock()
	res := d.res
	d.mu.Unlock()
	return res
}

func (d *Demo2) Write1(res int) {
	d.mu.Lock()
	d.res = res
	d.mu.Unlock()
}

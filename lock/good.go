package lock

import "sync"

type Demo1 struct {
	mu  sync.Mutex // 共享资源的锁
	res int        // 共享资源
}

func (d *Demo1) setRes(res int) {
	d.mu.Lock()
	defer d.mu.Unlock()
	d.res = res
}

func (d *Demo1) getRes() int {
	d.mu.Lock()
	defer d.mu.Unlock()
	return d.res
}

func (d *Demo1) Read1() int {
	res := d.getRes()
	return res
}

func (d *Demo1) Read2() int {
	res := d.getRes()
	return res
}

func (d *Demo1) Write1(res int) {
	d.setRes(res)
}

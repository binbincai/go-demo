package proto

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bon-backend/go-cli/httpclient"
	"gitlab.com/coinsea/go-pkg/http/binding"
	"gitlab.com/coinsea/go-pkg/http/render"
)

func TestFormUnmarshal(t *testing.T) {
	assert := require.New(t)

	svr := httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			// 测试参数解析
			req := FormDemo{}
			err := binding.Form.Bind(r, &req)
			assert.Nil(err, fmt.Sprint(err))
			_ = render.JSON{}.Render(w)
		}),
	)
	defer svr.Close()

	param := url.Values{}
	param.Set("intVal1", "1")
	param.Set("strVal1", "1")
	param.Set("strVal2", "AA")
	param.Set("strVal3", "")
	param.Set("strVal4", "BTC-VST")
	// param.Set("strVal4", "BTC\t-VST")
	param.Set("arrVal1", "1,2")
	param.Set("arrVal2", "AA,BB")
	param.Set("arrVal3", "BTC-USDT,BTC-VST")

	urlStr := fmt.Sprintf("%s?%s", svr.URL, param.Encode())
	httpclient.GetV2[int](context.TODO(), urlStr)
}

func TestJsonUnmarshal(t *testing.T) {
	assert := require.New(t)

	svr := httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			// 测试参数解析
			req := JsonDemp{}
			err := binding.JSON.Bind(r, &req)
			assert.Nil(err, fmt.Sprint(err))

			_ = render.JSON{}.Render(w)
		}),
	)
	defer svr.Close()

	param := JsonDemp{}
	param.IntVal1 = 1
	param.StrVal1 = "a"
	param.StrVal2 = "AA"
	param.StrVal4 = "BTC-USDT"
	param.ArrVal1 = []int{1, 2}
	param.ArrVal2 = []string{"AA", "BB"}
	param.ArrVal3 = []string{"BTC-USDT", "ETH-USDT"}

	body, _ := json.Marshal(param)
	_, err := httpclient.Post[JsonDemp](context.TODO(), svr.URL, body)
	assert.Nil(err)
}

func TestJsonUnmarshal2(t *testing.T) {
	assert := require.New(t)

	svr := httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			// 测试参数解析
			req := JsonDemp2{}
			err := binding.JSON.Bind(r, &req)
			assert.Nil(err, fmt.Sprint(err))

			// 需要手动验证子数组
			err = binding.Validator.ValidateStruct(req.ArrVal3)
			assert.Nil(err, fmt.Sprint(err))

			_ = render.JSON{}.Render(w)
		}),
	)
	defer svr.Close()

	param := JsonDemp2{}
	param.ArrVal3 = []SubDemo{
		{
			IntVal: 1,
			StrVal: "AA",
		},
	}

	body, _ := json.Marshal(param)
	_, err := httpclient.Post[JsonDemp](context.TODO(), svr.URL, body)
	assert.Nil(err)
}

package proto

// 可以用测试用例 TestFormUnmarshal 测试校验
type FormDemo struct {
	// 数值相关
	IntVal1 int `form:"intVal1" validate:"gt=0"`  // intVal1 需要 大于0
	IntVal2 int `form:"intVal2" validate:"gte=0"` // intVal2 需要 大于等于0
	// 字符串相关
	StrVal1 string `form:"strVal1" validate:"min=1,max=5"`                                                        // strVal1 长度范围 [1,5]
	StrVal2 string `form:"strVal2" validate:"oneof=AA BB"`                                                        // strVal2 取值只能是 AA BB
	StrVal3 string `form:"strVal3" validate:"len=0|oneof=AA BB"`                                                  // strVal3 为空 或 值为AA或BB
	StrVal4 string `form:"strVal4" validate:"omitempty,printascii,excludes= ,lt=15,endswith=-USDT|endswith=-VST"` // strVal4 币对名称专用
	// 数组相关
	ArrVal1 []int    `form:"arrVal1,split" validate:"min=1,dive,gt=0"`                                                               // arrVal1 数组不能为空, 逗号分隔多个元素, 每个元素的值需要大于0
	ArrVal2 []string `form:"arrVal2,split" validate:"min=2,dive,oneof=AA BB"`                                                        // arrVal2 数组不能为空, 逗号分隔多个元素, 每个元素的值只能是AA或BB
	ArrVal3 []string `form:"arrVal3,split" validate:"min=2,dive,omitempty,printascii,excludes= ,lt=15,endswith=-USDT|endswith=-VST"` // arrVal3 数组不能为空, 逗号分隔多个元素, 每个元素的值是币对
}

// 跟FormDemo区别:
//  1. tag form替换为json
//  2. 数组不需要指定split, 例如ArrVal1和ArrVal2
type JsonDemp struct {
	// 数值相关
	IntVal1 int `json:"intVal1" validate:"gt=0"`  // intVal1 需要 大于0
	IntVal2 int `json:"intVal2" validate:"gte=0"` // intVal2 需要 大于等于0
	// 字符串相关
	StrVal1 string `json:"strVal1" validate:"min=1,max=5"`                                                        // strVal1 长度范围 [1,5]
	StrVal2 string `json:"strVal2" validate:"oneof=AA BB"`                                                        // strVal2 取值只能是 AA BB
	StrVal3 string `json:"strVal3" validate:"len=0|oneof=AA BB"`                                                  // strVal3 为空 或 值为AA或BB
	StrVal4 string `json:"strVal4" validate:"omitempty,printascii,excludes= ,lt=15,endswith=-USDT|endswith=-VST"` // strVal4 币对名称使用这个
	// 数组相关
	ArrVal1 []int    `json:"arrVal1" validate:"min=1,dive,gt=0"`                                                               // arrVal1 数组不能为空, 逗号分隔多个元素, 每个元素的值需要大于0
	ArrVal2 []string `json:"arrVal2" validate:"min=2,dive,oneof=AA BB"`                                                        // arrVal2 数组不能为空, 逗号分隔多个元素, 每个元素的值只能是AA或BB
	ArrVal3 []string `json:"arrVal3" validate:"min=2,dive,omitempty,printascii,excludes= ,lt=15,endswith=-USDT|endswith=-VST"` // arrVal3 数组不能为空, 逗号分隔多个元素, 每个元素的值是币对
}

// 测试包含数组, 数组的元素是另外一个结构体的场景
type JsonDemp2 struct {
	ArrVal3 []SubDemo `json:"arrVal3" validate:"min=1"`
}

type SubDemo struct {
	IntVal int    `json:"intVal" validate:"gt=0"`
	StrVal string `json:"strVal" validate:"oneof=AA BB"`
}
